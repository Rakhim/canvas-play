(function() {

    document
        .getElementById('clear-canvas-button')
        .addEventListener('mousedown', mousedownListener, false);

    function mousedownListener() {
        document
            .getElementById('first-canvas')
            .dispatchEvent(events.clearEvent);
    }
})()