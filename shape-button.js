(function() {

    document
        .getElementById('shape-canvas-button')
        .addEventListener('mousedown', mousedownListener, false);

    function mousedownListener() {        
        document
            .getElementById('first-canvas')
            .dispatchEvent(events.shapeEvent);
    }
})()