(function() {

    document
        .getElementById('first-canvas-outline-button')
        .addEventListener('mousedown', mousedownListener, false);

    function mousedownListener() {
        document
            .getElementById('first-canvas')
            .dispatchEvent(events.outlineEvent);
    }
})()