var events = {
    "outlineEvent": function() {
        var event = document.createEvent('Event')
        event.initEvent("outline-event");
        return event;
    }(),
    "shapeEvent": function() {
        var event = document.createEvent('Event')
        event.initEvent("shape-event");
        return event;
    }(),
    "clearEvent": function() {
        var event = document.createEvent('Event')
        event.initEvent("clear-event");
        return event;
    }()
};
