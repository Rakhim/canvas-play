(function() {
    function getFirstCanvas() {
        return document.getElementById("first-canvas");
    }

    // init canvas
    (function () {
        var canvas = getFirstCanvas();
        canvas.width = 500
        canvas.height = 500
    
        canvas.addEventListener('mousedown', mousedownListener, false);
        canvas.addEventListener(events.outlineEvent.type, wheelMouseButton, false);
        canvas.addEventListener(events.shapeEvent.type, shapeByAngles, false);
        canvas.addEventListener(events.clearEvent.type, clearAll, false);    
    })();

    const dots = [];

    // очистить множество точек и поле.
    function clearAll() {
        clearCanvas();
        clearDots();
    }

    // очистить множество точек
    function clearDots() {
        dots.length = 0;
    }

    // очистить поле
    function clearCanvas(e) {
        var canvas = getFirstCanvas();
        canvas
            .getContext("2d")
            .clearRect(0, 0, canvas.width, canvas.height);
    }

    //прорисовка точки
    function drawDot(dot) {
        drawRadiusDot(dot, 2);
    }

    //прорисовка точки заданного радиуса
    function drawRadiusDot(dot, radius) {
        var canvas = getFirstCanvas().getContext("2d");
        canvas.beginPath();
        canvas.arc(dot.x, dot.y, radius, 0, 2 * Math.PI);
        canvas.stroke();
    }

    //прорисовка точек
    function drawDots(dotList) {
        var canvas = getFirstCanvas().getContext("2d");
        dotList.forEach(drawDot);
    }

    // получить самую левую/нижнюю точку.
    function getMostLeftBottomDot(dotList) {
        var result = {
            "x": dotList[0].x,
            "y": dotList[0].y
        };

        for (i = 1; i < dotList.length;i++) {
            var it = dotList[i];
            if (it.x <= result.x && it.y <= result.y) {
                result = it;
            }
        }

        return result;
    }

    // соединить точки согласно углам
    function shapeByAngles(e) {
        clearCanvas();
        sortByAngles(dots);

        //прорисовка согласно по отсортированным углам
        var context = getFirstCanvas().getContext('2d');
        context.beginPath();
        for (i = 0; i < dots.length; i++) {
            context.arc(dots[i].x, dots[i].y, 2, 0, 2 * Math.PI, false);
        }
        context.stroke();

        //
        var mostleft = getMostLeftBottomDot(dots);        
        drawRadiusDot(mostleft, 5);
    }


    // выделить выпуклый многогуольник
    function shapeDots(e) {
                
        var context = getFirstCanvas().getContext('2d');
        context.beginPath();
        for (i = 0; i < dots.length; i++) {
            for (j = 0; j < dots.length; j++) {
                for (k = 0; k < dots.length; k++) {
                    context.arc(dots[i].x, dots[i].y, 2, 0, 2 * Math.PI, false);
                    context.arc(dots[k].x, dots[k].y, 2, 0, 2 * Math.PI, false);
                    context.arc(dots[j].x, dots[j].y, 2, 0, 2 * Math.PI, false);
                }
            }
        }
        context.stroke();
    };

    // listener нажатия кнопок мыши
    function mousedownListener(e) {
        if (e.which == 1) {
            leftMouseButton(this, e);
        }
    }

    // получение угла вектора {x, y} относительно центра getCenterAsMean
    function getAngle(x, y) {
        return getVectorAngle({
            "x": x,
            "y": y
        });
    }

    // получение угла вектора {x, y}
    function getVectorAngle(dot) {
        if (dot.y == 0) {
            return 0;
        }            

        var angle = Math.atan(dot.x / dot.y);
        if (dot.x >= 0 && dot.y >= 0) {
            return angle;
        }

        if (dot.x >= 0 && dot.y < 0) {
            return Math.PI - angle;
        }

        if (dot.x < 0 && dot.y < 0) {
            return Math.PI + angle;
        }

        return 2 * Math.PI - angle;
    }

    // сортировка по углам
    function sortByAngles(dotList) {
        return dotList.sort(function(left, right) {
            return left.angle - right.angle;
        });
    }

    //обработчик нажатия левой кнопки мыши
    function leftMouseButton(el, e) {        
        var thisCanvas = el.getContext("2d");
        thisCanvas.beginPath();
        thisCanvas.arc(e.clientX, e.clientY, 2, 0, 2 * Math.PI);
        thisCanvas.stroke()

        dots.push({
            "x": e.clientX,
            "y": e.clientY,
            "angle": getAngle(e.clientX, e.clientY)
        });
    }

    //обработчик нажатия кнопки-колесика
    function wheelMouseButton(e) {
        e.preventDefault();

        clearCanvas();
        drawDots(dots);

        if (dots.length < 2) {
            return;
        }

        var center = getCenterAsMean();
        var thisCanvas = this.getContext("2d");
        thisCanvas.beginPath();
        thisCanvas.arc(center.x, center.y, 3, 0, 2 * Math.PI);
        thisCanvas.fillStyle = "red"
        thisCanvas.fill()

        thisCanvas.beginPath();
        thisCanvas.arc(center.x, center.y, farestDotDistance(center, dots), 0, 2 * Math.PI);
        thisCanvas.fillStyle = "green"
        thisCanvas.stroke()
    }

    function getCenterAsMean() {
        var x = 0, y = 0;
        var dot;
        for (i in dots) {
            dot = dots[i];
            x += dot.x;
            y += dot.y;
        }

        x /= dots.length;
        y /= dots.length;

        return {
            "x": x,
            "y": y
        };
    }

    function getCenterViaDiameter() {
        var diameterDescription = diameter(dots);
        var left = diameterDescription.left;
        var right = diameterDescription.right;
        return {
            "x": (right.x + left.x) / 2,
            "y": (right.y + left.y) / 2
        };
    }

    // эквивалентны ли точки
    function eq(left, right) {
        return left.x == right.x && left.y == right.y
    }

    //расстояние между двумя точками
    function distance(x, y) {
        if (eq(x, y)) {
            return 0;
        }

        return Math.sqrt((x.x - y.x)**2 + (x.y - y.y)**2);
    }

    // диаметр множества точек - максимально удаленные точки
    function diameter(dotList) {
        if (dotList.length < 2)
            return {
                "diameter": 0,
                "left": null,
                "right": null
            };

        var leftDot = dotList[0];
        var rightDot = dotList[1];
        var maxDistance = distance(leftDot, rightDot);
        var currentDistance = 0;
        var x, y;

        if (dotList.length == 2) {
            return {
                "diameter": maxDistance,
                "left": leftDot,
                "right": rightDot
            };
        }

        for (i in dotList) {
            for (j in dotList) {
                x = dotList[i];
                y = dotList[j];
                currentDistance =  distance(x, y);

               if (maxDistance < currentDistance) {
                    maxDistance = currentDistance;
                    leftDot = x;
                    rightDot = y;
                }
            }
        }

        return {
            "diameter": maxDistance,
            "left": leftDot,
            "right": rightDot
        };
    }

    // самая удаленная точка
    function farestDotDistance(x, dotList) {
        var i = 0;
        if (dotList.length == 0) {
            return 0;
        }
        
        var currentDistance = 0;
        var maxDistance = 0;
        for (i in dotList) {
            currentDistance = distance(x, dotList[i])
            if (maxDistance < currentDistance) {
                maxDistance = currentDistance;
            }
        }

        return maxDistance;
    }
})();